require "bitbucket_api_client/version"
require "bitbucket_api_client/configuration"
require "bitbucket_api_client/api"
require "bitbucket_api_client/base"
require "bitbucket_api_client/repository"
require "bitbucket_api_client/pull_request"
require "bitbucket_api_client/comment"

module Bitbucket
  class << self
    def configure
      yield config
    end

    def config
      @config ||= Configuration
    end
  end
end
