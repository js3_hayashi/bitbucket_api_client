module Bitbucket
  class PullRequest < Base
    attr_reader :id, :title, :branch

    class << self
      def endpoint(repo_slug = nil, username = nil)
        repo_slug ||= config.repo_slug
        username ||= config.username
        "repositories/#{username}/#{repo_slug}/pullrequests"
      end
    end

    # PullRequest.new(JSON.load(json_string))
    #   or
    # PullRequest.new(pull_request_id, repo_slug, username)
    #   or
    # PullRequest.new(pull_request_id, repo_slug)
    #   or
    # PullRequest.new(pull_request_id)
    def initialize(*args)
      if args[0].kind_of?(Hash)
        data = args[0]
      elsif args.size <= 3
        id, repo_slug, username = args[0, 3]
        repo_slug ||= config.repo_slug
        username  ||= config.username
        raise ArgumentError if [id, repo_slug, username].include?(nil)

        endpoint = "/" + self.class.endpoint(repo_slug, username) + "/#{id}"
        data = request(:get, endpoint)
      else
        raise ArgumentError
      end

      @title    = data["title"]
      @branch   = data["source"]["branch"]["name"]
      @endpoint = data["links"]["self"]["href"].match(/repositories\/.+/)[0]
    end

    def username
      @endpoint.split("/")[1]
    end

    def repo_slug
      @endpoint.split("/")[2]
    end

    def id
      @endpoint.split("/")[4].to_i
    end

    # コメントの一覧を返す
    #
    def comments
      @comments ||= (
        comments = request(:get, "comments")
        comments.collect do |c|
          Comment.new c
        end
      )
    end

    # プルリクエストにコメントを追加する
    #
    # @param content [String] コメント
    # @return [Bitbucket::Comment] 成功すれば Comment のインスタンスを返す
    def add_comment(content)
      Comment.add(content, id, repo_slug)
    end
  end
end
