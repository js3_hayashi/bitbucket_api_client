require 'faraday'
require 'json'

module Bitbucket
  class Api
    def config
      Bitbucket.config
    end

    def request(method, endpoint, params = {}, opts = {})
      raise unless endpoint[0] == "/"
      params ||= {}

      uri = base(opts) + endpoint
      #puts "Base#request: #{method.upcase} #{uri} #{params.inspect}"

      res = http_client.send(method) do |req|
        req.url uri
        if %i(post put).include?(method)
          req.headers['Content-Type'] = 'application/json'
          req.body = params.to_json
        else
          req.params = params
        end
      end
      #puts "Base#request: code: #{res.status}, body: #{res.body.force_encoding('utf-8')}"
      unless res.success?
        raise StandardError, "method: #{method}, uri: #{uri}, code: #{res.status}, body: #{res.body.force_encoding('utf-8')}"
      end

      parse_json(res.body) do |next_page|
        params["page"] = next_page
        request(method, endpoint, params, opts)
      end
    end

    private

    def base(opts = {})
      ver = opts[:v] || opts[:ver] || opts[:version] || "2.0"
      "/#{ver}"
    end

    def http_client
      Faraday.new "https://api.bitbucket.org" do |conn|
        conn.basic_auth(*config.basic_auth)   if config.basic_auth
        conn.request :url_encoded
        conn.adapter(Faraday.default_adapter)
      end
    end

    # JSON 形式の文字列を ruby のオブジェクトに変換する
    #
    # pagination が true のときはすべてのページの .values を結合した値を返す。
    # json が paginated なデータではないときは無視される。
    #
    # https://developer.atlassian.com/bitbucket/api/2/reference/meta/pagination
    def parse_json(json, pagination = true)
      data = JSON.load json.force_encoding('utf-8')

      if pagination && %w(size page pagelen values).select{|k| data.has_key?(k)}.size == 4
        values = data["values"]
        if data.has_key?("next")
          # get next page values
          values += yield data["page"] + 1
        end
        values
      else
        data
      end
    end
  end
end
