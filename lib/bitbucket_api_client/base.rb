module Bitbucket
  class Base
    attr_reader :endpoint

    def config
      self.class.config
    end

    def initialize
      raise "Abstract Class !!"
    end

    # endpoint が絶対パス(スラッシュで始まっている)のときはエンドポイントとして利用する
    # endpoint が相対パスのときは self.endpoint からの相対パスとして利用する
    def request(method, endpoint = nil, data = nil, opts = {})
      unless endpoint.to_s[0] == "/"
        endpoint = ["", self.endpoint, endpoint].compact.join("/")
      end

      self.class.request(method, endpoint, data, opts)
    end

    def reload
      raise "Abstract method !!"
    end

    class << self
      def config
        Bitbucket.config
      end

      def request(method, endpoint, data = nil, opts = {})
        api = Bitbucket::Api.new
        api.request(method, endpoint, data, opts)
      end
    end
  end
end
