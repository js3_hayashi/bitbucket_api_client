module Bitbucket
  module Configuration
    class << self
      attr_accessor :basic_auth
      attr_accessor :username
      attr_accessor :repo_slug
    end
  end
end
