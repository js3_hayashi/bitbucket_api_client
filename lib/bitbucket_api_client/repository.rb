module Bitbucket
  class Repository < Base
    class << self
      def endpoint(username = nil)
        username ||= config.username
        "repositories/#{username}"
      end

      def list
        #TODO
        raise "todo"
      end
    end

    def initialize(*args)
      repo_slug, username = args[0, 2]
      repo_slug ||= config.repo_slug
      username  ||= config.username
      raise ArgumentError if [repo_slug, username].include?(nil)

      @endpoint  = self.class.endpoint(username) + "/#{repo_slug}"
    end

    def username
      @endpoint.split("/")[1]
    end

    def repo_slug
      @endpoint.split("/")[2]
    end

    def pull_requests
      pull_requests = request(:get, 'pullrequests')
      pull_requests.collect do |pr|
        PullRequest.new pr
      end
    end
  end
end
