module Bitbucket
  class Comment < Base
    class << self
      def endpoint(pull_request_id, repo_slug = nil, username = nil)
        repo_slug ||= config.repo_slug
        username  ||= config.username
        "repositories/#{username}/#{repo_slug}/pullrequests/#{pull_request_id}/comments"
      end

      def add(content, pull_request_id, repo_slug = nil, username = nil)
        repo_slug ||= config.repo_slug
        username  ||= config.username
        res = request(:post,
                      "/" + endpoint(pull_request_id, repo_slug, username),
                      {content: {raw: content}}
                     )
        new(res)
      end
    end


    # Comment.new(JSON.load(json_string))
    #   or
    # Comment.new(comment_id, pull_request_id, repo_slug, username)
    #   or
    # Comment.new(comment_id, pull_request_id, repo_slug)
    #   or
    # Comment.new(comment_id, pull_request_id)
    def initialize(*args)
      if args[0].kind_of?(Hash)
        data = args[0]
      elsif args.size <= 4
        id, pull_request_id, repo_slug, username = args[0, 4]
        repo_slug ||= config.repo_slug
        username  ||= config.username
        raise ArgumentError if [id, pull_request_id, repo_slug, username].include?(nil)

        endpoint = "/" + self.class.endpoint(pull_request_id, repo_slug, username) + "/#{id}"
        data = request(:get, endpoint)
      else
        raise ArgumentError
      end

      @content  = data["content"]
      @endpoint = data["links"]["self"]["href"].match(/repositories\/.+/)[0]
    end

    def username
      @endpoint.split("/")[1]
    end

    def repo_slug
      @endpoint.split("/")[2]
    end

    def pull_request_id
      @endpoint.split("/")[4].to_i
    end

    def id
      @endpoint.split("/")[6].to_i
    end

    def to_s(format = 'raw')
      @content[format]
    end

    #FIXME v2.0対応とテストコードの実装
    def update(content)
      comment = request :put, nil, {content: {raw: content}}
      self.class.new comment
    end
  end
end
