require 'spec_helper'

describe Bitbucket::Comment do
  let(:repository) { Bitbucket::Repository.new('bitbucket_api_client') }
  let(:pull_request) { repository.pull_requests.first }

  context '#update' do
    it 'コメントを編集できること' do
      string = Time.now.to_s
      expect(pull_request.comments.last.update(string).to_s).to eq string
    end
  end
end

