require 'spec_helper'

describe Bitbucket::Repository do
  let(:repository) { Bitbucket::Repository.new('bitbucket_api_client') }
  context '.new' do
    it 'インスタンスが生成されること' do
      expect(repository).to be_kind_of Bitbucket::Repository
    end
  end
  context '#pull_requests' do
    it 'Bitbucket::PullRequest の配列を返すこと' do
      expect(repository.pull_requests).to be_kind_of Array
      expect(repository.pull_requests.first).to be_kind_of Bitbucket::PullRequest
    end
  end
end
