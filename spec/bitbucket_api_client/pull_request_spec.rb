require 'spec_helper'

describe Bitbucket::PullRequest do
  let(:repository) { Bitbucket::Repository.new('bitbucket_api_client') }
  let(:pull_request) { repository.pull_requests.first }

  context '.new' do
    it 'インスタンスが生成されること' do
      pull_request = Bitbucket::PullRequest.new(1, 'bitbucket_api_client', "js3_hayashi")
      expect(pull_request).to be_kind_of Bitbucket::PullRequest
    end
  end
  context '#id' do
    it 'pull_request_idを返すこと' do
      expect(pull_request.id).to eq 1
    end
  end
  context '#branch' do
    it 'branchを返すこと' do
      expect(pull_request.branch).to eq "example"
    end
  end
  context '#comments' do
    it 'Bitbucket::Comment の配列を返す' do
      expect(pull_request.comments).to be_kind_of Array
      expect(pull_request.comments.first).to be_kind_of Bitbucket::Comment
    end
  end
  context '#add_comment' do
    it 'コメントが追加できること' do
      expect(pull_request.add_comment("コメント by rspec")).to be_kind_of Bitbucket::Comment
    end
  end
end
