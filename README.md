# BitbucketApiClient

ruby から Bitbucket API を利用するためのライブラリです。

Bitbucket API で得た JSON String は ruby のオブジェクト（HashやArray）に変換されます。

いまのところ必要とする機能しか実装されていません。

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bitbucket_api_client'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install bitbucket_api_client

## Usage

リポジトリ、プルリクエスト、プルリクエストのコメントを得る。

```ruby
require "bitbucket_api_client"

username = "js3_hayashi"
repo_slug = "bitbucket_api_client"

repo = Bitbucket::Repository.new repo_slug, username
repo.pull_requests
#=> [#<Bitbucket::PullRequest:0x00000002647d30 @id=1, @title="本Gemのテスト用のブランチの最初のコミット", @branch="example", @endpoint="repositories/js3_hayashi/bitbucket_api_client/pullrequests/1">]

pull_request_id = 1
pull_request = Bitbucket::PullRequest.new pull_request_id, repo_slug, username
#=> #<Bitbucket::PullRequest:0x00000002647d30 @id=1, @title="本Gemのテスト用のブランチの最初のコミット", @branch="example", @endpoint="repositories/js3_hayashi/bitbucket_api_client/pullrequests/1">

comment = pull_request.comments.first
#=> #<Bitbucket::Comment:0x00000002898788 @id=43943292, @pull_request_id=1, @content={"raw"=>"最初のコメント", "markup"=>"markdown", "html"=>"<p>最初のコメント</p>"}, @endpoint="repositories/js3_hayashi/bitbucket_api_client/pullrequests/1/comments/43943292">

comment.to_s
#=> "最初のコメント"
```

プルリクエストにコメントを追加する。

```ruby
require "bitbucket_api_client"

username = "js3_hayashi"
repo_slug = "bitbucket_api_client"
pull_request_id = 1

pull_request = Bitbucket::PullRequest.new pull_request_id, repo_slug, username
pull_request.add_comment "これはコメントです。"
#=> Instance of Bitbucket::Comment
```

## Configuration

ブロック形式と関数形式の両方に対応しています。

```ruby
Bitbucket.configure do |config|
  config.username = "js3_hayashi"
end
```

または

```ruby
Bitbucket.config.username = "js3_hayashi"
```

### basic_auth

```ruby
Bitbucket.config.basic_auth = ["hayashi@jcch-sss.com", "パスワード"]
```

プライベートリポジトリの参照やコメントの投稿などは認証情報を設定しておく必要があります。

ブラウザで `https://bitbucket.org/` にログインする際の認証情報を設定します。

### username と repo_slug

```ruby
Bitbucket.configure do |config|
  config.username = "js3_hayashi"
  config.repo_slug = "bitbucket_api_client"
end
```

リポジトリの所有者と名前です。以下の通り対応しています。

    https://bitbucket.org/js3_hayashi/bitbucket_api_client
    git clone git@bitbucket.org:js3_hayashi/bitbucket_api_client.git

これらの値は同名の引数でデフォルト値として利用されます。

特定のリポジトリにしかアクセスしないアプリケーションを作る場合に設定ファイルなどで事前に指定することを想定しています。

```ruby
# config/bitbucket.rb
Bitbucket.configure do |config|
  config.username = "js3_hayashi"
  config.repo_slug = "bitbucket_api_client"
end
```

```ruby
# your application code
repo = Bitbucket::Repository.new   # 引数を省略可能
pull_requests = repo.pull_requests
```
